FROM node:lts  As build
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY --chown=node:node package*.json ./
RUN npm i
COPY --chown=node:node . .
RUN npm run build
ENV NODE_ENV production
USER node

FROM node:lts  As prod
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY --chown=node:node package*.json ./
RUN npm i
COPY --chown=node:node . .
RUN npm run build
ENV NODE_ENV production
USER node
CMD [ "node", "dist/main.js" ]