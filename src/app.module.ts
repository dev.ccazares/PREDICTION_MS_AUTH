import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { Auth0Module } from './guards/auth0/auth0.module';
import { AuthModule } from './modules/auth/auth.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    Auth0Module,
    AuthModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
