// jwt-auth.guard.ts
import { Injectable, Logger, UnauthorizedException } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

@Injectable()
export class Auth0Guard extends AuthGuard('jwt') {
  logger = new Logger(Auth0Guard.name);

  handleRequest(err, user, info: Error) {
    if (err || info || !user) {
      this.logger.error(err);
      this.logger.error(info);
      throw err || new UnauthorizedException();
    }
    return user;
  }
}
