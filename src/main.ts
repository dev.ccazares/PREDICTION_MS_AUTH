import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Logger } from '@nestjs/common';
const logger = new Logger('Auth');

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true });
  await app.listen(process.env.PORT ? parseInt(process.env.PORT) : 3000);
  logger.log(`Microservice is listening on: ${await app.getUrl()}`);
}
bootstrap();
