import { Controller, Get, UseGuards, Req } from '@nestjs/common';
import { Auth0Guard } from 'src/guards/auth0/auth0.guard';

@UseGuards(Auth0Guard)
@Controller('auth')
export class AuthController {
  @Get('validate-token')
  valid(@Req() req) {
    return req.user;
  }
}
